FROM node:16 as frontend-build

WORKDIR /app/

COPY package*.json ./

RUN npm install

COPY *.js ./
COPY src ./src
COPY public ./public

ARG PREFIX=/
ENV VUE_APP_PUBLIC_URL=$PREFIX
ENV VUE_APP_BACKEND_URL=$PREFIX

RUN npm run build

FROM ubuntu

COPY --from=frontend-build /app/dist /app/dist

WORKDIR /app/

RUN apt-get update && \
	apt-get install -y python3-flask curl cmake && \
	curl -L https://github.com/ReimuNotMoe/ydotool/archive/refs/tags/v1.0.4.tar.gz -o ydotool.tar.gz && \
	tar -xzf ydotool.tar.gz && \
	cd ydotool-1.0.4 && \
	echo > manpage/CMakeLists.txt && \
	cmake . && \
	make && \
	make install && \
	cd .. && \
	rm -rf ydotool* && \
	apt-get purge -y curl cmake

COPY server.py ./

CMD /app/server.py

