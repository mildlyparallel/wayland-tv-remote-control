#!/usr/bin/env python3

import os
import sys
import json
import flask
import subprocess

input_codes_path = "/usr/include/linux/input-event-codes.h"

http = flask.Flask(
    __name__,
    static_url_path = '',
    static_folder = 'dist'
)

if http.config['DEBUG'] and 'flask_cors' in sys.modules:
    from flask_cors import CORS
    CORS(http)

def read_keymap(filename):
    keymap = {}
    for line in open(filename, 'r'):
        l = line.strip()
        if not l.startswith('#define'):
            continue
        tokens = l.split()
        if len(tokens) < 3:
            continue

        key = tokens[1]

        if not key.startswith("KEY_"):
            continue

        value = tokens[2]

        keymap[key] = value

    for k in list(keymap.keys()):
        v = keymap[k]
        if v.startswith('KEY_'):
            keymap[k] = keymap[v]

        try:
            code = int(keymap[k], 16)
        except:
            del keymap[k]

    return keymap

def action_to_command(act):
    cmd = ['ydotool']

    if 'key' in act:
        cmd.append('key')
        keys = act['key'].split()

        codes = []
        for k in keys:
            codekey = f'KEY_{k.upper()}'
            if codekey not in keymap:
                raise Exception(f'Unknown key: {k}')
            codes.append(keymap[codekey])

        for c in codes:
            cmd.append(f'{c}:1')
        for c in reversed(codes):
            cmd.append(f'{c}:0')

        return cmd

    if 'type' in act:
        cmd += ['type', act['type']]
        return cmd

    if 'click' in act:
        cmd.append('click')

        if act['click'] == 'middle':
            cmd += ['0x42', '0x82']
        if act['click'] == 'right':
            cmd += ['0x41', '0x81']
        if act['click'] == 'left':
            cmd += ['0x40', '0x80']
        return cmd

    if 'touch' in act:
        x = act['touch'].get('x', 0)
        y = act['touch'].get('y', 0)
        cmd += ['mousemove', '--', str(x), str(y)]
        return cmd

    if 'scroll' in act:
        x = act['scroll'].get('x', 0)
        y = act['scroll'].get('y', 0)
        cmd += ['mousemove',  '-w', '--', str(x), str(y)]
        return cmd

@http.route('/')
def get_index():
    return flask.send_file('dist/index.html')

@http.route('/do', methods = ['POST'])
def post_press():
    req = flask.request.json

    cmd = action_to_command(req)
    http.logger.debug(req)
    http.logger.debug(' '.join(cmd))

    sp = subprocess.run(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)

    res = {
        'rc': sp.returncode,
        'stdout': sp.stdout.decode('utf-8'),
        'stderr': sp.stderr.decode('utf-8')
    }

    if sp.returncode != 0:
        http.logger.error(req)
        http.logger.error(cmd)
        http.logger.error(res)
    else:
        http.logger.debug(res)

    return res



keymap = read_keymap(input_codes_path)

if __name__ == '__main__':
    port = os.environ.get('FLASK_RUN_PORT', '5000')
    host = os.environ.get('FLASK_RUN_HOST', '0.0.0.0')

    srv = None
    if os.environ.get('START_YDOTOOLD'):
        srv = subprocess.Popen('ydotoold')

    try:
        http.run(port = port, host = host)
    finally:
        if srv:
            srv.kill()

