import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify);

export default new Vuetify({
	theme: { 
		dark: true,
		themes: {
			light: {
				touchpadColor: colors.grey.lighten2,
				scrollpadColor: colors.grey.lighten3,
			},
			dark: {
				touchpadColor: colors.grey.darken3,
				scrollpadColor: colors.grey.darken2,
			}
		},
	},
	icons: {
		iconfont: 'md',
	},
});

