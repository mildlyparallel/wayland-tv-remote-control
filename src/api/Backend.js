import axios from 'axios';

const url = require('url');

const Backend = {
	resolve: function(path) {
		let r = process.env.VUE_APP_BACKEND_URL || process.env.VUE_APP_PUBLIC_URL || '/';
		return url.resolve(r, path);
	},

	call: function(method, url, formData, jsonData, params) {
		console.debug(method, url, formData, jsonData, params);

		return new Promise((resolve, reject) => {
			var data = null;

			if (formData) {
				data = new FormData();
				for (const d of formData) {
					var k = Object.keys(d)[0];
					var v = d[k];
					data.append(k, v);
				}
			} else if (jsonData) {
				data = jsonData;
			}

			axios({
				method: method,
				url: this.resolve(url),
				data: data,
				params: params,
			}).then((res) => {
				if (res.status == 200) {
					console.debug('reply:', res.status);
					resolve(res.data);
				} else {
					console.error('reply:', res);
					reject(res);
				}
			}).catch((error) => {
				console.error('call error:', error);
				reject(error);
			});
		});
	},

	get: function(url, params) {
		return this.call('GET', url, null, params, null);
	},

	postForm: function(url, data, params) {
		return this.call('POST', url, data, params, null);
	},

	postJson: function(url, data, params) {
		return this.call('POST', url, null, data, params);
	},

	delete: function(url, params) {
		return this.call('DELETE', url, null, params, null);
	},
};

export { Backend };


