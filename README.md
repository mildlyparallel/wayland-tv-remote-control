# Wayland/X11 TV Remote Control

**waylandtv** is a remote control service with mobile web-interface for Linux TV
home theater.

*by Ruslan Kuchumov, 2023*

### Features

- Play and audio control buttons,
- Browser tabs, history and windows navigation,
- Touchpad and scrolling
- Keyboard text input

### Screenshots

![tv remote light theme](docs/screenshot_01.png){width=20%}
![tv remote dark theme](docs/screenshot_02.png){width=20%}
![tv remote more btn](docs/screenshot_04.png){width=20%}
![tv remote settings](docs/screenshot_03.png){width=20%}

waylandtv works by calling [ydotool](https://github.com/ReimuNotMoe/ydotool)
commands in its backend.

### Deployment using docker

Build the container:

```bash
docker build -t waylandtv .
```

Start the service:

```bash
docker run \
    -p 8080:5000 \
    -e START_YDOTOOLD=1 \
    --device /dev/uinput:/dev/uinput \
    -t waylandtv
```

The service would listen on 8080 port of your local address.

### Installation

Install nodejs dependencies and compile frontend static files:

```bash
npm install
npm run build
```

> You can specify path prefix with `VUE_APP_PUBLIC_URL` environment variable
> before compilation, e.g. `export VUE_APP_PUBLIC_URL=/tv-remote/` 

Install backend dependencies:

```bash
pip3 install -r requirements.txt
```

Make sure [ydotool](https://github.com/ReimuNotMoe/ydotool) is installed and
its version is v1.0.4 or above (otherwise mouse scroll feature would not work).

Ydotool should work under a regular user privileges. To achieve that, you may
change the owner of ydotool client unix-socket file:

```bash
chown $USER: /tmp/.ydotool_socket
```

Before starting the backend service, you may need to specify ydotool socket path
and service listening address in the environment variables:

```bash
export YDOTOOL_SOCKET=/tmp/.ydotool_socket
export FLASK_RUN_PORT=5000
export FLASK_RUN_HOST=0.0.0.0
```

To start the backend service run `server.py` script:

```bash
python3 ./server.py
```

The service would listen on 5000 port on all your addresses. Do not forget to
restrict the unwanted access to it with third-party tools.

